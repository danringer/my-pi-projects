#!/usr/bin/env python

from time import sleep
import os
import RPi.GPIO as GPIO

# 0     left tailfin
# 1     left wing
# 2     upper fuselage
# 3     lower fuselage
# 4     right tailfin
# 5     engine bit
# 6     right wing
# 7     door
# 8     door
# 9     door
# 10    door
# 11    tail highlight
# 12    right wing highlight
# 13    left wing highlight
# 14    engine bit
# 15    window (always cyan)
# 16    engine bit (always black)

DOT_PINS = (1, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26)

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

#will need to check whether pulling up or down and True vs false with dev unit

for pin in DOT_PINS:
    GPIO.setup(pin, GPIO.IN, GPIO.PUD_UP)

try:
    while True:
            if ( GPIO.input(1) == False ):
                os.system('mpg321 one.mp3 &')
            if ( GPIO.input(4) == False ):
                os.system('mpg321 two.mp3 &')
            if ( GPIO.input(5) == False ):
                os.system('mpg321 three.mp3 &')
            if ( GPIO.input(6) == False ):
                os.system('mpg321 four.mp3 &')
            if ( GPIO.input(7) == False ):
                os.system('mpg321 five.mp3 &')
            if ( GPIO.input(8) == False ):
                os.system('mpg321 six.mp3 &')
            if ( GPIO.input(9) == False ):
                os.system('mpg321 seven.mp3 &')
            if ( GPIO.input(11) == False ):
                os.system('mpg321 eight.mp3 &')
            if ( GPIO.input(12) == False ):
                os.system('mpg321 nine.mp3 &')
            if ( GPIO.input(13) == False ):
                os.system('mpg321 ten.mp3 &')                
            if ( GPIO.input(14) == False ):
                os.system('mpg321 eleven.mp3 &')  
            if ( GPIO.input(15) == False ):
                os.system('mpg321 twelve.mp3 &')
            if ( GPIO.input(16) == False ):
                os.system('mpg321 thirteen.mp3 &')
            if ( GPIO.input(17) == False ):
                os.system('mpg321 fourteen.mp3 &')   
            if ( GPIO.input(18) == False ):
                os.system('mpg321 fifteen.mp3 &')  
            if ( GPIO.input(20) == False ):
                os.system('mpg321 sixteen.mp3 &')
            if ( GPIO.input(21) == False ):
                os.system('mpg321 seventeen.mp3 &')
            if ( GPIO.input(22) == False ):
                os.system('mpg321 eighteen.mp3 &')  
            if ( GPIO.input(23) == False ):
                os.system('mpg321 nineteen.mp3 &')
            if ( GPIO.input(24) == False ):
                os.system('mpg321 twenty.mp3 &')
            if ( GPIO.input(25) == False ):
                os.system('mpg321 twentyone.mp3 &')                
            if ( GPIO.input(26) == False ):
                os.system('mpg321 twentytwo.mp3 &')
#                    print "button pressed"
            sleep(.5);

finally:
    GPIO.cleanup()

