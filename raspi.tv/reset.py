#!/usr/bin/env python2.7
# by Alex Eames of http://RasPi.TV 

import RPi.GPIO as GPIO
import sys, os
from time import sleep

GPIO.setmode(GPIO.BCM)                                  # initialise RPi.GPIO

GPIO.setup(25, GPIO.OUT)                      # set up ports for output

answer = raw_input("Are you sure you want to reset? y/n \n")

if answer != "y" and answer != "yes":
    print "OK. Not resetting. To change your mind, rerun the program"
    GPIO.cleanup()
    sys.exit()

try:
    print "OK. Syncing file system, then resetting."
    command = os.system("sync")
    if command == 0:
        print "sync successful - resetting now"
        sleep(1)
        GPIO.output(25, 1)
        sleep(10)
        print "I think you forgot to connect your wires. Connect them up and rerun the program."

except KeyboardInterrupt:         # trap a CTRL+C keyboard interrupt
    GPIO.cleanup()
GPIO.cleanup()
